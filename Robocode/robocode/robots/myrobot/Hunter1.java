package myrobot;

import robocode.*;
import robocode.Robot;
import java.awt.*;

public class Hunter1 extends AdvancedRobot {
    boolean movingForward;
    int moveDirection = 1;
	public static double PI = Math.PI;
	Enemy enemy = new Enemy();
    public void run() {
	    
        //如果 flag 被设置成 true，那么坦克车转动时，炮保持原来的方向。
        setAdjustGunForRobotTurn(true);
        //设置坦克颜色
        setBodyColor(Color.blue);
        //设置炮塔颜色
        setGunColor(Color.green);
        //设置雷达颜色
        setRadarColor(Color.pink);
        //设置扫描区域颜色
        setScanColor(Color.green);
        //设置子弹颜色
        setBulletColor(Color.white);
        //如果 flag 被设置成 true，那么坦克车（和炮）转动时，雷达会保持原来的方向。
        setAdjustRadarForGunTurn(true);

        while (true) {
            movingForward = true;
            turnRadarRightRadians(5);
            if (enemy.name == null) {
                setTurnRadarRightRadians(2 * PI);
                execute();
            } else {
                execute();

            }
        }

    }
    public void onHitWall(HitWallEvent e) {

        //moveDirection = -moveDirection;
        reverseDirection();
    }

    public void reverseDirection() {
        if (movingForward) {
            setBack(10000);
            movingForward = false;
        } else {
            setAhead(10000);
            movingForward = true;
        }
    }

    public void onHitRobot(HitRobotEvent e) {
       /* if (e.isMyFault()) {
            reverseDirection();
			setBack(100);
            fire(2);
        }*/
	   moveDirection=-moveDirection;
	   fire(2);
       /* setBack(200);
        setFire(2);
        turnRight(45);*/
    }

    public void onBulletHit(BulletHitEvent event) {
        double energy = event.getEnergy();
        if (energy < event.getEnergy()) {
            //moveDirection = -moveDirection;
            turnRight(45);
            setFire(2);
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {

        enemy.update(e, this);
        double Offset = rectify(enemy.direction - getRadarHeadingRadians());
        //注意 这里的计算都以弧度为单位
        double absBearing = 0d;

        //雷达转动角度
        double radarTurnAngle = 0d;

        double gunTurnAngle = 0d;

        double bodyTurnAngle = 0d;

        //得到绝对角度差
        absBearing = robocode.util.Utils.normalRelativeAngle(e.getBearingRadians() + getHeadingRadians());

        //根据absBearing角算出Radar要调整的角度
        radarTurnAngle = Math.sin(absBearing - getRadarHeadingRadians());

        gunTurnAngle = Math.sin(absBearing - getGunHeadingRadians());

        bodyTurnAngle = Math.sin(absBearing - getHeadingRadians());

        //转动雷达，注意是向右
        setTurnRadarRightRadians(radarTurnAngle);

        //转动炮管
        setTurnGunRightRadians(gunTurnAngle);

        //转动身体
        setTurnRightRadians(bodyTurnAngle);

        //前进
        setAhead(e.getDistance());

        if (getGunHeat() < 0.1) setFire(2);
    }
	
public double rectify(double angle) {
        if (angle < -Math.PI)

            angle += 2 * Math.PI;

        if (angle > Math.PI)

            angle -= 2 * Math.PI;

        return angle;

    }
	
public class Enemy {
        public double x, y;

        public String name = null;

        public double headingRadian = 0.0D;

        public double bearingRadian = 0.0D;

        public double distance = 1000D;

        public double direction = 0.0D;

        public double velocity = 0.0D;

        public double prevHeadingRadian = 0.0D;

        public double energy = 100.0D;


        public void update(ScannedRobotEvent e, AdvancedRobot Hunter) {
            name = e.getName();

            headingRadian = e.getHeadingRadians();

            bearingRadian = e.getBearingRadians();

            this.energy = e.getEnergy();

            this.velocity = e.getVelocity();

            this.distance = e.getDistance();

            direction = bearingRadian + Hunter.getHeadingRadians();

            x = Hunter.getX() + Math.sin(direction) * distance;

            y = Hunter.getY() + Math.cos(direction) * distance;

        }

    }
}


